import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import './index.css'

//NOTE - Reactjs Hooks
import { useState, useEffect } from 'react'

//NOTE - NextUI Items
import {Button, Input, Radio, RadioGroup, cn, Textarea, Card, CardBody, Divider, Dropdown, DropdownTrigger, DropdownMenu, DropdownItem, Spinner, Switch} from "@nextui-org/react";

//NOTE - Font awesome
import { faFloppyDisk, faRetweet, faPaperPlane, faCopy, faTrash, faChampagneGlasses, faCircleQuestion, faBrain, faSliders, faEllipsisVertical} from '@fortawesome/free-solid-svg-icons';
import { faReact } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

function App() {

  let gpt_max_tokens = {
    "gpt-3.5-turbo": 4000,
    "gpt-3.5-turbo-16k": 16000,
    "gpt-4": 8000,
    "gpt-4-32k": 32000
  }

  //NOTE - UseState to save the diferents status
  const [apikey, setApikey] = useState("")
  const [apikeysaved, setApikeysaved] = useState("false")
  const [gptSelected, setGptSelected] = useState("")
  const [userText, setUserText] = useState("")
  const [gptText, setGptText] = useState("")
  const [lang, setLang] = useState("EN")
  const [spinner, setSpinner] = useState(false)
  const [remember, setRemember] = useState(false)
  const [conversation, setConversation] = useState([])
  const [tokens, setTokens] = useState(0)

  //NOTE - UseEffect that init two useState with the data getted in localStorage
  const start = useEffect(()=>{
    setApikey(localStorage.getItem("apikey"))
    setApikeysaved(localStorage.getItem("apikeysaved"))
    setGptSelected(localStorage.getItem("gptselected") || "gpt-3.5-turbo")
    setLang(localStorage.getItem("lang") || "EN")
  }, [])

  //NOTE - Function that set the apikey in a useState and save it in localStorage
  const func_set_apikay = (e) => {
    const currentApiKey = e.target.value;
    setApikey(currentApiKey);
    localStorage.setItem("apikey", currentApiKey);
  }

  //NOTE - Function that set a "true" in a useState and save it in localStorage
  const func_apikey_saved = () => {
    setApikeysaved("true")
    localStorage.setItem("apikeysaved", "true");
  }

  //NOTE - Function that set a "false" in a useState and save it in localStorage
  const funct_see_apikey = () => {
    setApikeysaved("false")
    localStorage.setItem("apikeysaved", "false");
  }

  //NOTE - Function that set the selected gpt model in a useState and save it in localStorage
  const save_radio_selected = (e) => {
    setGptSelected(e.target.value)
    localStorage.setItem("gptselected", e.target.value);
  }

  const get_gpt_answer = () => { 
    let all_conversation = ""

    if(remember){
      let sumatory_tokens = tokens + userText.split("").length
    
      if(sumatory_tokens > gpt_max_tokens[gptSelected]){      
        while(sumatory_tokens > gpt_max_tokens[gptSelected]){        
          sumatory_tokens -= conversation[0]['tokens']
          setTokens(prevTokens => prevTokens - conversation[0]['tokens'])
          setConversation(prevConversation => prevConversation.slice(1))
        }
      }

      all_conversation = lang == "ES" ? `A continuación encontrarás interacciones previas. Por favor, tómalas en cuenta para proporcionar respuestas coherentes y para recordar conversaciones pasadas. No utilices el formato [Usuario] o [ChatGPT] en tus respuestas. La próxima pregunta es: ` : `Below you will find previous interactions. Please consider them to provide coherent answers and to recall past conversations. Do not use the [User] or [ChatGPT] format in your responses. The next question is:`

      all_conversation += conversation.map((conv) => `rem(${conv['conversation']})`).join(' ')

      all_conversation += `\n\n${userText}`
    }else{
      all_conversation = userText
    }    

    setSpinner(true)
    fetch('https://api.openai.com/v1/chat/completions', {
      method: 'POST',
      headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${apikey}`,
      },
      body: JSON.stringify({
          model: gptSelected,
          messages: [
              {
                  "role": "system",
                  "content": "You are a helpful assistant."
              },
              {
                  "role": "user",
                  "content": all_conversation,
              }
          ]
      }),
    })
    .then(response => response.json())
    .then(data => {
        setSpinner(false)
        setGptText("")
        console.log(data)
        
        // setTokens(prevTokens => prevTokens - conversation[0]['tokens'])
        setTokens(data['usage']['total_tokens'])
        
        setConversation(prevConv => [...prevConv, {
          'tokens': data['usage']['prompt_tokens'],
          'conversation': `${lang == "ES" ? "[Usuario]" : lang=="EN" && "[User]"}: ${userText}`
        }]);
        
        setConversation(prevConv => [...prevConv, {
          'tokens': data['usage']['completion_tokens'],
          'conversation': `[ChatGPT]: ${data['choices'][0]['message']['content']}`
        }]);

        setGptText(data['choices'][0]['message']['content'])
    })
    .catch((error) => {
      setSpinner(false)
      setGptText(lang== "ES" ? "Api no funcional" : "Not functional api")
  });
}

  //NOTE - Function that save clicked language
  const set_lang = (clicked_lang) => {
    setLang(clicked_lang)
    localStorage.setItem("lang", clicked_lang);
  }

  //NOTE - Function that copy Textarea text
  const copy_text = (origin) => {
    if(origin == "chat"){
      navigator.clipboard.writeText(gptText);
    }else if(origin == "user"){
      navigator.clipboard.writeText(userText);
    }
  }

  //NOTE - Function that delete text in Textarea
  const delete_text = (origin) => {
    if(origin == "chat"){
      setGptText("")
    }else if(origin == "user"){
      setUserText("")
    }
  }

  return (
    <section className='bg-gray-900'>  
      <Card className={`absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 z-30 ${spinner ? 'block' : 'hidden'}`}>
        <CardBody>
          <Spinner color="warning" size="lg"/>
          <p className='pt-5 font-semibold text-center'>
            <FontAwesomeIcon className='text-xl pe-2' icon={faChampagneGlasses} />
            {lang == "ES" ? "Los datos fueron enviados" : lang=="EN" && "The data was sent"}
          </p>
          <p className='pt-3'>{lang == "ES" ? "Esperando la respuesta de la API de OpenAI" : lang=="EN" && "Waiting for the response from the OpenAI API"}</p>
        </CardBody>
      </Card>    
      <div className='p-5 bg-gray-900'>
        <div className=' flex justify-between'>
            <div className='flex justify-start items-center'>
              <Card className={lang != "ES" ? 'bg-opacity-20 cursor-pointer' : 'bg-opacity-60 cursor-pointer'}>
                <CardBody onClick={() => set_lang("ES")} className='p-2'>
                  <p className='font-semibold'>ES</p>
                </CardBody>
              </Card>

              <Divider className='mx-3 bg-white bg-opacity-20 h-9' orientation="vertical" />

              <Card className={lang != "EN" ? 'bg-opacity-20 cursor-pointer' : 'bg-opacity-60 cursor-pointer'}>
                <CardBody onClick={() => set_lang("EN")} className='p-2'>
                  <p className='font-semibold'>EN</p>
                </CardBody>
              </Card>
              <div className='ms-5'>
                <Switch
                  size="lg"
                  color="warning"
                  onChange={()=> {setRemember(!remember)}}
                  value={remember}
                  thumbIcon={({ isSelected, className }) =>
                    isSelected ? (
                      <FontAwesomeIcon className=' text-lg' icon={faBrain} />
                    ) : (
                      <FontAwesomeIcon className=' text-lg' icon={faCircleQuestion} />
                    )
                  }
                >
                  {/* <p className=' text-white'>{remember ? (lang == "ES" ? "Recordar Conversacion" : "Remember Conversation") : (lang == "ES" ? "No Recordar Conversacion" : "No Remember Conversation")}</p> */}
                </Switch>
              </div>
              {remember && (<p className='text-white ms-4'>{tokens} - {gpt_max_tokens[gptSelected]}</p>)}              
              
            </div>
            <div className='flex justify-end items-center'>
              {apikeysaved=="false" && <Input size="sm" className='me-3' type="password" value={apikey ? apikey : ""} label="ApiKey" placeholder="Introduzca su ApiKey" onChange={func_set_apikay} />}
              {apikeysaved=="false" ? (
                <Button onClick={func_apikey_saved} color="warning" size="lg" variant="ghost">
                  <FontAwesomeIcon className=' text-lg' icon={faFloppyDisk} />
                </Button>  
              ) : (
                <Button onClick={funct_see_apikey} color="warning" size="lg" variant="ghost">
                  <FontAwesomeIcon className=' text-lg' icon={faRetweet} />
                </Button> 
              )}
            </div>           
        </div>
        <div className='flex justify-center items-center py-20'>
          <RadioGroup color="warning" value={gptSelected} onChange={save_radio_selected} className='custom-radio-group'>
            <CustomRadio value="gpt-3.5-turbo">
              GPT-3.5 Turbo  
            </CustomRadio>
            <CustomRadio value="gpt-3.5-turbo-16k">
              GPT-3.5 16K  
            </CustomRadio>
            <CustomRadio value="gpt-4">
              GPT-4 8K   
            </CustomRadio>
            <CustomRadio value="gpt-4-32k">
              GPT-4 32K
            </CustomRadio>
          </RadioGroup>
        </div>
        <div className=' flex flex-col justify-around items-center'>
          <div className='w-full flex flex-col justify-center items-center'>
            <div  className='w-full max-w-7xl flex justify-end pb-1'>
              <Dropdown className=' bg-slate-800 bg-opacity-95'>
                <DropdownTrigger>
                  <Button variant="ghost" color="warning" size="sm">
                    <FontAwesomeIcon size='xl' icon={faEllipsisVertical} />
                  </Button>
                </DropdownTrigger>
                <DropdownMenu aria-label="Dynamic Actions">
                    <DropdownItem
                      key="1"
                      className="text-success font-semibold"
                      onClick={() => copy_text("chat")}
                    >
                      <FontAwesomeIcon className='pe-2' icon={faCopy} />{lang == "ES" ? "Copiar Texto" : lang=="EN" && "Copy Text"}
                    </DropdownItem>

                    <DropdownItem
                      key="2"
                      className="text-danger font-semibold"
                      onClick={() => delete_text("chat")}
                    >
                      <FontAwesomeIcon className='pe-2' icon={faTrash} />{lang == "ES" ? "Borrar Texto" : lang=="EN" && "Delete Text"}
                    </DropdownItem>                  
                </DropdownMenu>
              </Dropdown>
            </div>
            <Textarea
              label={lang == "ES" ? "Texto ChatGPT" : "ChatGPT Text"}
              labelPlacement="inside"
              minRows={15}
              maxRows={15}
              className="max-w-7xl mb-5"
              value={gptText}
              isReadOnly
            />
          </div>

          <div className='w-full flex flex-col justify-center items-center'>
            <div className='w-full max-w-7xl flex justify-end pb-1'>
              <Dropdown className=' bg-slate-800 bg-opacity-95'>
                <DropdownTrigger>
                  <Button variant="ghost" color="warning" size="sm">
                    <FontAwesomeIcon size='xl' icon={faEllipsisVertical} />
                  </Button>
                </DropdownTrigger>
                <DropdownMenu aria-label="Dynamic Actions">
                    <DropdownItem
                      key="1"
                      className="text-success font-semibold"
                      onClick={() => copy_text("user")}
                    >
                      <FontAwesomeIcon className='pe-2' icon={faCopy} />{lang == "ES" ? "Copiar Texto" : lang=="EN" && "Copy Text"}
                    </DropdownItem>

                    <DropdownItem
                      key="2"
                      className="text-danger font-semibold"
                      onClick={() => delete_text("user")}
                    >
                      <FontAwesomeIcon className='pe-2' icon={faTrash} />{lang == "ES" ? "Borrar Texto" : lang=="EN" && "Delete Text"}
                    </DropdownItem>   
                </DropdownMenu>
              </Dropdown>
            </div>
            <Textarea
              label={lang == "ES" ? "Tu texto" : "Your Text"}
              labelPlacement="inside"
              minRows={1}
              className="max-w-7xl mb-5"
              value={userText}
              onChange={(e) => setUserText(e.target.value)}
            />
          </div>
        
          <Button onClick={get_gpt_answer} className=' w-73' color="warning" size="lg" variant="ghost">
            <FontAwesomeIcon className='' icon={faPaperPlane} /> {lang=="ES" ? "Enviar Texto" : "Send Text"}
          </Button> 
        </div>
        <div className='w-full mt-5'>
          <Divider className='my-3 bg-white bg-opacity-20 w-full' orientation="horizontal" />
        </div>
        <div className='w-full flex gap-2 sm:gap-0 justify-around sm:justify-between items-center flex-wrap'>
          <div className='text-white text-opacity-80'>© 2023</div>
          <div className='text-white text-opacity-80 flex justify-around items-center'>
            <div className='pe-4'>
              <FontAwesomeIcon className='text-xl pe-2' icon={faReact} />
              ReactJS
            </div>
            <div className='flex justify-around items-center pe-4'>
              <svg className="pe-2 opacity-80 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 24 24">
                <path fill="#FFFFFF" d="M11.5725 0c-.1763 0-.3098.0013-.3584.0067-.0516.0053-.2159.021-.3636.0328-3.4088.3073-6.6017 2.1463-8.624 4.9728C1.1004 6.584.3802 8.3666.1082 10.255c-.0962.659-.108.8537-.108 1.7474s.012 1.0884.108 1.7476c.652 4.506 3.8591 8.2919 8.2087 9.6945.7789.2511 1.6.4223 2.5337.5255.3636.04 1.9354.04 2.299 0 1.6117-.1783 2.9772-.577 4.3237-1.2643.2065-.1056.2464-.1337.2183-.1573-.0188-.0139-.8987-1.1938-1.9543-2.62l-1.919-2.592-2.4047-3.5583c-1.3231-1.9564-2.4117-3.556-2.4211-3.556-.0094-.0026-.0187 1.5787-.0235 3.509-.0067 3.3802-.0093 3.5162-.0516 3.596-.061.115-.108.1618-.2064.2134-.075.0374-.1408.0445-.495.0445h-.406l-.1078-.068a.4383.4383 0 01-.1572-.1712l-.0493-.1056.0053-4.703.0067-4.7054.0726-.0915c.0376-.0493.1174-.1125.1736-.143.0962-.047.1338-.0517.5396-.0517.4787 0 .5584.0187.6827.1547.0353.0377 1.3373 1.9987 2.895 4.3608a10760.433 10760.433 0 004.7344 7.1706l1.9002 2.8782.096-.0633c.8518-.5536 1.7525-1.3418 2.4657-2.1627 1.5179-1.7429 2.4963-3.868 2.8247-6.134.0961-.6591.1078-.854.1078-1.7475 0-.8937-.012-1.0884-.1078-1.7476-.6522-4.506-3.8592-8.2919-8.2087-9.6945-.7672-.2487-1.5836-.42-2.4985-.5232-.169-.0176-1.0835-.0366-1.6123-.037zm4.0685 7.217c.3473 0 .4082.0053.4857.047.1127.0562.204.1642.237.2767.0186.061.0234 1.3653.0186 4.3044l-.0067 4.2175-.7436-1.14-.7461-1.14v-3.066c0-1.982.0093-3.0963.0234-3.1502.0375-.1313.1196-.2346.2323-.2955.0961-.0494.1313-.054.4997-.054z"></path>
              </svg>
              NextUI
            </div>
            <div className='flex justify-around items-center pe-4'>
              <svg className="pe-2 opacity-80 h-6 flex justify-center items-center" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 70 70" fill="#ffffff" >
                <path d="M23.3 13.756c-8.363-1.523-14.976.07-17.612 4.636-1.967 3.406-1.405 7.923 1.363 12.75a.86.86 0 0 0 1.171.317.86.86 0 0 0 .317-1.171c-2.493-4.35-2.978-8.247-1.366-11.04 2.2-3.792 8.088-5.214 15.82-3.806a.86.86 0 0 0 .997-.69.86.86 0 0 0-.69-.997zM11.4 37c3.382 3.716 7.775 7.202 12.786 10.095C36.328 54.1 49.243 55.98 55.6 51.64a.86.86 0 0 0 .225-1.192.86.86 0 0 0-1.192-.225c-5.697 3.884-17.966 2.106-29.6-4.605-4.86-2.806-9.115-6.182-12.375-9.764a.86.86 0 0 0-1.212-.057.86.86 0 0 0-.057 1.212zm42.178-1.033c5.476-6.47 7.388-12.976 4.758-17.53-1.935-3.352-6.026-5.122-11.464-5.192a.86.86 0 0 0-.869.846.86.86 0 0 0 .846.869c4.9.064 8.415 1.588 10 4.335 2.185 3.784.48 9.586-4.582 15.566a.86.86 0 0 0 .1 1.209.86.86 0 0 0 1.209-.1zM39.504 14.04c-4.95 1.06-10.222 3.135-15.283 6.057C11.697 27.327 3.527 38 4.66 45.72a.86.86 0 0 0 .973.724.86.86 0 0 0 .724-.973c-1.007-6.864 6.737-16.97 18.72-23.9 4.9-2.835 10.013-4.843 14.785-5.866a.86.86 0 0 0 .659-1.018.86.86 0 0 0-1.018-.659z"/>
                <path d="M19.215 51.082C22.078 59.077 26.762 64 32.03 64c3.842 0 7.396-2.62 10.167-7.23a.86.86 0 0 0-.293-1.177.86.86 0 0 0-1.177.293c-2.5 4.143-5.55 6.397-8.697 6.397-4.375 0-8.553-4.4-11.2-11.78a.86.86 0 0 0-1.097-.518.86.86 0 0 0-.518 1.097zm26.153-1.564c1.482-4.737 2.278-10.2 2.278-15.895 0-14.208-4.973-26.456-12.056-29.6a.86.86 0 0 0-1.132.436.86.86 0 0 0 .436 1.132C41.212 8.395 45.93 20.02 45.93 33.624c0 5.524-.77 10.816-2.2 15.383a.86.86 0 0 0 .562 1.075.86.86 0 0 0 1.075-.562zm16.152-.84c0-2.267-1.838-4.105-4.105-4.105S53.3 46.4 53.3 48.678s1.838 4.105 4.105 4.105 4.105-1.838 4.105-4.105zm-1.715 0a2.39 2.39 0 0 1-2.389 2.389 2.39 2.39 0 0 1-2.389-2.389 2.39 2.39 0 0 1 2.389-2.389 2.39 2.39 0 0 1 2.389 2.389z"/>
                <path d="M6.584 52.783c2.267 0 4.105-1.838 4.105-4.105s-1.838-4.105-4.105-4.105a4.1 4.1 0 1 0 .001 8.21zm0-1.715a2.39 2.39 0 0 1-2.389-2.389 2.39 2.39 0 0 1 2.389-2.389 2.39 2.39 0 0 1 2.389 2.389 2.39 2.39 0 0 1-2.389 2.389zM32.03 8.2c2.267 0 4.105-1.838 4.105-4.105S34.296 0 32.03 0s-4.105 1.838-4.105 4.105S29.763 8.2 32.03 8.2zm0-1.715a2.39 2.39 0 0 1-2.389-2.389 2.39 2.39 0 0 1 2.389-2.389 2.39 2.39 0 0 1 2.389 2.389 2.39 2.39 0 0 1-2.389 2.389zm.626 30.04a2.97 2.97 0 1 1-1.254-5.806 2.97 2.97 0 1 1 1.254 5.806z"/>
              </svg>
              ElectronJS
            </div>          
          </div>
        </div>
      </div>      
    </section>
  )
}

export const CustomRadio = (props) => {
  const {children, ...otherProps} = props;

  return (
    <Radio
      {...otherProps}
      classNames={{
        base: cn(
          "m-0 bg-content1 hover:bg-content2 hover:bg-opacity-80",
          "max-w-[300px] cursor-pointer rounded-lg gap-4 p-4 border-2 border-transparent",
          "data-[selected=true]:border-warning bg-opacity-20"
        ),
      }}
    >
      {children}
    </Radio>
  );
};

export default App
